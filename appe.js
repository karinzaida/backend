// SERVIDOR QUE VA A CONSUMIR MLAP

var express = require('express')
var bodyParser = require('body-parser')
var app = express()

var requestJson = require('request-json')

var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';

app.use(bodyParser.json())
//https://bitbucket.org/oyara/api3ed/src/b04b9debcee8f717f232a1e5b3b60b7f11d3546c/server.js?at=master&fileviewer=file-view-default
//https://api.mlab.com/api/1/databases/bdapiperu/collections/user?apiKey=ILQ4EvViJQjbaLiY3Z8526GHW-eITWa_
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdapiperu/collections"
var apiKey = "apiKey=ILQ4EvViJQjbaLiY3Z8526GHW-eITWa_"
var clienteMlab

//GET users
app.get(URLbase + 'users', function(req, res) {
  /*clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  }) */
  clienteMlab = requestJson.createClient(urlMlabRaiz);
  console.log("Cliente HTTP MLAP creado");
  var queryString = 'f={"_id":0}&l=2000&';
  clienteMlab.get(urlMlabRaiz + '/user?' + queryString + apiKey, function(err, resM, body) {
    var response = {};
    if (err) {
      response = {
        "msg ": "Error al obtener usuario"
      };
      res.status(resM.statusCode);
    } else {
      if (body.length > 0) {
        response = body;
        res.status(resM.statusCode);
      } else {
        response = {
          "msg": "No se encontraron usuarios"
        };
        res.status(resM.statusCode);
      }
    }
    res.send(response);
  });
})

//GET user by ID
app.get(URLbase + 'users/:id', function(req, res) {
  var id = req.params.id;
  var query = 'q={"userID":' + id + '}&f={"_id":0}&';
  clienteMlab = requestJson.createClient(urlMlabRaiz);
  //  https://api.mlab.com/api/1/databases/bdapiperu/collections/user?q={"userID":1}&apiKey=ILQ4EvViJQjbaLiY3Z8526GHW-eITWa_
  clienteMlab.get(urlMlabRaiz + '/user?' + query + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length > 0) {
        res.send(body[0])
        res.status(resM.statusCode);
        //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
      } else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
});


//POST - AÑADIR USUARIO
app.post(URLbase + 'users', function(req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz);
  let createdUser = {
    "userID": req.body.userID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password,
    "ip_address": req.body.ip_address
  };
  var response = {};
  clienteMlab.post(urlMlabRaiz + '/user?' + apiKey, createdUser, function(err, resM, body) {
    if (err) {
      response = {
        "msg": "Error añadiendo usuario."
      }
      res.send(response);
      res.status(resM.statusCode);
    } else {
      response = {
        "msg": "Usuario añadido correctamente."
      }
      res.status(resM.statusCode);
      res.send(response);
    }

  });
});

//PUT of user - tutor
app.put(URLbase + 'users/:id', function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"userID":' + id + '}&';
  clienteMlab = requestJson.createClient(urlMlabRaiz);
  //  clienteMlab = requestJSON.createClient(urlMlabRaiz);
  clienteMlab.get(urlMlabRaiz + '/user?' + queryStringID + apiKey, function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    clienteMlab.put(urlMlabRaiz + '/user?q={"userID": ' + id + '}&' + apiKey, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:" + body);
        res.send(body);
      });
  });

});

// Petición PUT con id de mLab (_id.$oid) --FUNCIONA
app.put(URLbase + 'usersmLab/:id', function(req, res) {
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"userID":' + id + '}&';
  var httpClient = requestJson.createClient(urlMlabRaiz);

  httpClient.get(urlMlabRaiz + '/user?' + queryString + apiKey,
    function(err, respuestaMLab, body) {
      let response = body[0];
      console.log(body);

      //Actualizo los campos del usuario
      let updatedUser = {
        "userID": req.body.userID,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password,
        "ip_address": req.body.ip_address
      }; //Otra forma simplificada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);

      //Llamo al put de mlab.
      httpClient.put(urlMlabRaiz + '/user/' + response._id.$oid + '?' + apiKey, updatedUser,
        function(err, respuestaMLab, body) {
          var response = {};
          if (err) {
            response = {
              "msg": "Error actualizando usuario."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body;
            } else {
              response = {
                "msg": "Usuario actualizado correctamente."
              }
              res.status(404);
            }
          }
          res.send(response);
        });
    });
});

//DELETE user with id - tutor
app.delete(URLbase + "users/:id", function(req, res) {
  console.log("entra al DELETE");
  console.log("request.params.id: " + req.params.id);
  var id = req.params.id;
  var queryStringID = 'q={"userID":' + id + '}&';
  console.log(urlMlabRaiz + 'user?' + queryStringID + apiKey);
  var httpClient = requestJson.createClient(urlMlabRaiz);
  httpClient.get(urlMlabRaiz + '/user?' + queryStringID + apiKey,
    function(error, respuestaMLab, body) {
      var respuesta = body[0];
      console.log("body delete:" + respuesta);
      httpClient.delete(urlMlabRaiz + "/user/" + respuesta._id.$oid + '?' + apiKey,
        function(error, respuestaMLab, body) {
          res.send(body);
        });
    });
});

//LOGIN
app.post(URLbase + 'logi', function(req, res) {
  //var email = req.headers.email;
  //var password = req.headers.password;
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey)
  let response
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Login ok
      {
        //console.log(resM);
        clienteMlab = requestJson.createClient(urlMlabRaiz)
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put(urlMlabRaiz + '/user' + '?q={"userID": ' + body[0].userID + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          res.send({
            "login": "ok",
            "userID": body[0].userID,
            "first_name": body[0].first_name,
            "last_name": body[0].last_name
          })
        })

      } else {
        response = {
          "msg": "Usuario no encontrado"
        }
        res.status(404).send(response)
      }
    }
  })
})
//LOGIN V2
app.post(URLbase + 'login', function(req, res) {
  //var email = req.headers.email;
  //var password = req.headers.password;
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"' + email + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey)
  let response
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // usuario existe
      {
        //console.log(resM);
        if (body[0].password == password) {
          clienteMlab = requestJson.createClient(urlMlabRaiz)
          var cambio = '{"$set":{"logged":true}}'
          clienteMlab.put(urlMlabRaiz + '/user' + '?q={"userID": ' + body[0].userID + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
            res.send({
              "login": "ok",
              "userID": body[0].userID,
              "first_name": body[0].first_name,
              "last_name": body[0].last_name
            })
          })
        } else {
          response = {
            "msg": "Contraseña incorrecta"
          }
          res.send(response);
        }
      } else {
        response = {
          "msg": "Usuario no encontrado"
        }
        res.status(404).send(response)
      }
    }
  })
})

//LOGOUT
//unset : false
app.post(URLbase + 'logout', function(req, res) {
  var id = req.body.userID;
  var query = 'q={"userID":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient(urlMlabRaiz)
  let response
  clienteMlab.get(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey, function(err, resM, body) {
    if (!err) {
      if (body.length == 1) // Estaba logado
      {
        clienteMlab = requestJson.createClient(urlMlabRaiz)
        //var cambio = '{"$set":{"logged":false}}'
        var cambio = '{"$unset":{"logged":true}}'
        clienteMlab.put(urlMlabRaiz + '/user' + '?q={"userID": ' + body[0].userID + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          res.send({
            "logout": "ok",
            "userID": body[0].userID
          })
        })
      } else {
        response = {
          "msg": "Usuario no logado previamente"
        }
        res.status(200).send(response)
      }
    }
  })
})


app.listen(port);
